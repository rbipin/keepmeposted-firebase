'use strict'
module.exports = class ChannelModel {
    constructor(channelId, title, feedUrl, groupBy, sortOrder) {
        this._id = channelId;
        this._title = title;
        this._feedUrl = feedUrl;
        this._groupBy = groupBy;
        this._sortOrder = sortOrder;
    }

    get channelId(){
        return this._id;
    }

    get title(){
        return this._title;
    }

    get feedUrl(){
        return this._feedUrl;
    }

    get groupBy(){
        return this._groupBy;
    }

    get sortOrder(){
        return this._sortOrder;
    }
};