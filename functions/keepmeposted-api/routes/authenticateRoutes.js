'use strict';
const router = require('express').Router({mergeParams: true});
let authenticationController=require('../controllers/authenticationController');

router.all("*",authenticationController.authenticateRequest);

module.exports = router;