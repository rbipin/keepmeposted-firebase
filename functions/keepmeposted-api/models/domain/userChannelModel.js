const customException = require('../../common/exceptions/customException');
let ChannelModel = require('../../models/value-object/channelModel');
let commonUtil = require('../../common/utils/commonUtils');

module.exports = class UserChannelModel {

    constructor(userId) {
        this._userId = userId;
    }
    get userId() {
        return this._userId;
    }

    //Public Method to Get all channel for the user
    getAllChannels() {
        const collectionName = 'channels';
        var firebaseApp = commonUtil.CreateFirebaseApp();
        var db = firebaseApp.firestore();
        var userChannels = db.collection(collectionName);
        //return a the promsise on performing the query operation on the collection
        return userChannels.where('userId', '==', this.userId)
            .get()
            .then((records) => {
                let channelList = [];
                records.forEach((doc) => {
                    let channelData = doc.data();
                    let channel = new ChannelModel(
                        channelData['channelId'],
                        channelData['title'],
                        channelData['feedUrl'],
                        channelData['groupBy'],
                        channelData['sortOrder']
                    );
                    channelList.push(channel);
                });
                return channelList;
            })
            .catch(err => {
                throw err;
            });
    }

    //Get the channel by the channel id for the user
    getChannelById(channelId) {
        const collectionName = 'channels';
        var firebaseApp = commonUtil.CreateFirebaseApp();
        var db = firebaseApp.firestore();
        var userChannels = db.collection(collectionName);
        //return a the promsise on performing the query operation on the collection
        return userChannels.where('userId', '==', this.userId)
            .where("channelId", "==", channelId).limit(1)
            .get()
            .then((records) => {
                if (!records.empty) {
                    let channelData = records.docs[0].data();
                    let channel = new ChannelModel(channelData['channelId'],
                        channelData['title'],
                        channelData['feedUrl'],
                        channelData['groupBy'],
                        channelData['sortOrder']);
                    return channel;
                } else {
                    throw new customException.NotFound(`Requested channel not found`);
                }
            })
            .catch(err => {
                throw err;
            });
    }

    //Add channel for the user
    addChannel(newChannel) {
        const collectionName = 'channels';
        var firebaseApp = commonUtil.CreateFirebaseApp();
        var db = firebaseApp.firestore();
        var userChannels = db.collection(collectionName);

        return userChannels.where("userId", "==", this.userId)
            .where("channelId", "==", newChannel.channelId)
            .get()
            .then(records => {
                if (records.empty) {
                    let data = {
                        userId: this.userId,
                        channelId: newChannel.channelId,
                        title: newChannel.title,
                        feedUrl: newChannel.feedUrl,
                        groupBy: '',
                        sortOrder: '',
                        createdTimestamp: new Date(),
                        lastUpdateTimeStamp: new Date()
                    }
                    return userChannels.add(data);
                } else {
                    throw new customException.ChannelAlreadyExistsException(newChannel.channelId, newChannel.title);
                }
            })
            .then(insertDocRef => {
                return userChannels.doc(insertDocRef.id).get();
            })
            .then(newDocument => {
                return newDocument.data();
            })
            .catch(err => {
                throw err;
            });
    }

    //Update the channel information
    updateChannel(channelModel) {

    }

    //Delete the channel informaion
    deleteChannel(channelId) {
        const collectionName = 'channels';
        var firebaseApp = commonUtil.CreateFirebaseApp();
        var db = firebaseApp.firestore();
        var userChannels = db.collection(collectionName);
        return userChannels.where('userId', '==', this.userId)
            .where('channelId', '==', channelId)
            .get()
            .then(records => {
                let channelList = [];
                records.forEach(doc => {
                    let channelData = doc.data();
                    let channel = new ChannelModel(channelData['channelId'],
                        channelData['title'],
                        channelData['feedUrl'],
                        channelData['groupBy'],
                        channelData['sortOrder']);
                    channelList.push(channel);
                    doc.ref.delete();
                });
                return channelList;
            })
            .catch(err => {
                throw err;
            });
    }
}