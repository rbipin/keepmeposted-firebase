/* eslint-disable promise/always-return */
'use strict';
const UserAccountModel = require('../models/domain/userAccountModel');
const UserModel = require('../models/value-object/userModel');
let commonUtils = require('../common/utils/commonUtils');

exports.getUserInformation = (request, response) => {
    let userId = request.params.userid;
    let userAccount = new UserAccountModel(userId);
    userAccount.getUserAccount()
        .then(userAcc => {
            response.send(userAcc);
        })
        .catch(exception => {
            commonUtils.HandleException(exception, response);
        });

}

exports.createUserProfile = (request, response) => {
    let userId = request.params.userid;
    const newUser = new UserModel(request.body.firstName,
        request.body.lastName,
        request.body.preferredName
    );
    let userAccount = new UserAccountModel(userId);
    userAccount.addUserAccount(newUser)
    .then(newUserInfo=>{
        response.send(newUserInfo);
    })
    .catch(exception=>{
        commonUtils.HandleException(exception,response);
    })
}

exports.updateUserProfile = (request, response) => {

}