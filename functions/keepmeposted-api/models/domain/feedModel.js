module.exports = class FeedModel {
    constructor(feedType, feedData) {
        this.feedType = feedType;
        this.feed = feedData;
    }
};