'use strict'

module.exports = class UserModel {
    constructor(firstName, lastName, preferredName) {
        this._firstName = firstName;
        this._lastName = lastName;
        this._preferredName = preferredName;
    }

    get firstName(){
        return this._firstName;
    }
};