/* eslint-disable promise/always-return */
'use strict';
let UserChannelModel = require('../models/domain/userChannelModel');
let ChannelModel = require('../models/value-object/channelModel');
let commonUtils = require('../common/utils/commonUtils');

//Get all the channels for the user
exports.getAllChannels = (request, response) => {
    let userid = request.params.userid;
    let userChannel = new UserChannelModel(userid);
    //Get all the channels
    userChannel.getAllChannels()
        .then((channelList) => {
            response.send(JSON.stringify(channelList));
        })
        .catch((err) => {
            commonUtils.HandleException(err, response);
        });
}

//Get a channels for the user by the channel id
exports.getAChannel = (request, response) => {

    let userId = request.params.userid;
    let channelId = request.params.channelid;
    let userChannel = new UserChannelModel(userId);
    //Get the channel by id
    userChannel.getChannelById(channelId)
        .then(channelList => {
            response.send(channelList);
        })
        .catch((err) => {
            commonUtils.HandleException(err, response);
        });
}

//Add a new channel for the user
exports.addChannel = (request, response) => {
    let userId = request.params.userid;
    const channelId = request.body.channelId;
    const title = request.body.title;
    const feedUrl = request.body.feedUrl;
    const groupBy = request.body.groupBy;
    const sortOrder = request.body.sortOrder;
    //Add a new channel
    let newChannel = new ChannelModel(channelId, title, feedUrl, groupBy, sortOrder);
    let userChannel = new UserChannelModel(userId);
 
        userChannel.addChannel(newChannel)
        .then(newInsertedRecord=>{
            response.send(newInsertedRecord);
        })
        .catch(err=>{
            commonUtils.HandleException(err,response);
        });
}

exports.updateChannel = (request, response) => {

}

exports.deleteChannel = (request, response) => {
    let userId = request.params.userid;
    let channelId = request.params.channelid;
    let userChannel = new UserChannelModel(userId);
    userChannel.deleteChannel(channelId)
        .then(channelList => {
            response.send(channelList);
        })
        .catch(err => {
            commonUtils.HandleException(err, response);
        })
}