'use strict';
var applicationException=require('./applicationException');

exports.InvalidFeedUriException=class InvalidFeedUriException extends applicationException{
    constructor(){
        super("This is an invalid RSS feed URL", 404);
    }
};

exports.ChannelAlreadyExistsException=class ChannelAlreadyExistsException extends applicationException{
    constructor(){
        super("Channel already Exists",409);
    }
}

exports.UserAlreadyExistsException=class UserAlreadyExistsException extends applicationException{
    constructor(){
        super("User already Exists",409);
    }
}

exports.NotFound=class NotFound extends applicationException{
    constructor(message){
        super(message,404);
    }
}