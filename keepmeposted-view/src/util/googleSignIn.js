/* eslint-disable no-unused-vars */
import * as firebase from "firebase/app";
import "firebase/auth"

function onSignIn() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then((result) => {
        var user = result.user;
        // eslint-disable-next-line no-console
        console.log(user);
    }).catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error.code);
        // eslint-disable-next-line no-console
        console.log(error.message);
    });
}