'use strict';
const router = require('express').Router({mergeParams: true});

let channelController = require('../controllers/channelController');

router.route('/')
.get(channelController.getAllChannels);

router.route('/')
.post(channelController.addChannel);

router.route('/:channelid')
.get(channelController.getAChannel);

router.route('/:channelid')
.patch(channelController.updateChannel);

router.route('/:channelid')
.delete(channelController.deleteChannel);

module.exports = router; 
