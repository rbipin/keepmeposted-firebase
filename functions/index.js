const functions = require('firebase-functions');
var express=require('express');
var bodyParser=require('body-parser');
var apiRouter=require('./keepmeposted-api/routes/apiRoutes');
app=express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api',apiRouter);

exports.keepmeposted = functions.https.onRequest(app);
