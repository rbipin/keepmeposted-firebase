'use strict';

const router = require('express').Router({mergeParams: true});

let feedController = require('../controllers/feedController');

router.route('/').post(feedController.getFeed);

module.exports = router;