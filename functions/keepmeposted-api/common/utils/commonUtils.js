'use strict'
const appException = require('../exceptions/applicationException');
const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('../../serviceAccountKey.json');
let fireBaseApp = undefined;

exports.HandleException = (exception, response) => {
    if (Object.getPrototypeOf(exception.constructor).name === appException.name) {
        response.status(exception.status)
            .send(exception.message);
    } else {
        response.status(500).send(exception.toString());
    }
};

exports.CreateFirebaseApp = () => {
    if (fireBaseApp === undefined) {
        fireBaseApp = firebaseAdmin.initializeApp({
            credential: firebaseAdmin.credential.cert(serviceAccount)
        });
        return fireBaseApp;
    } else {
        return fireBaseApp;
    }


};