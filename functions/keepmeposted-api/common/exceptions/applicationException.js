module.exports=class ApplicationException extends Error{
    constructor(message,status){
        super(message);
        // Set the name of the Error
        this.name=this.constructor.name;

        Error.captureStackTrace(this, this.constructor);
        
        //If Http Status code is passed then use the code else set 500
        this.status=status || 500;
    }
};