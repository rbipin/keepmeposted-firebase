'use strict'
const router = require('express').Router();

//Authenticate the requests
router.all('*', require('./authenticateRoutes'));

//Transfer control to feedRoutes
router.use('/feed', require('./feedRoutes'));

//Transfer control to User Routes
router.use('/users', require('./userRoutes'));

//Transfer control to Channel routes
router.use('/users/:userid/channels', require('./channelRoutes'));

module.exports = router;