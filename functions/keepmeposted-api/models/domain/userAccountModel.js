'use strict'
const UserModel = require('../value-object/userModel');
const customException = require('../../common/exceptions/customException');
let commonUtils = require('../../common/utils/commonUtils');

module.exports = class UserAccount {
    constructor(userid) {
        this._userId = userid;
    }

    //
    get userId() {
        return this._userId;
    }

    getUserAccount() {
        const collectionName = 'users';
        var firebaseApp = commonUtil.CreateFirebaseApp();
        var db = firebaseApp.firestore();
        var userAcc = db.collection(collectionName);
        return userAcc.where('userId', '==', this.userId).limit(1)
            .get()
            .then(records => {
                if (!records.empty) {
                    let userData = records.docs[0];
                    let user = new UserModel(userData['firstName'],
                        userData['lastName'],
                        userData['preferredName']
                    );
                    return user;
                } else {
                    throw new customException.NotFound(`User Account not found`);
                }
            })
            .catch(err => {
                throw err;
            });
    }

    addUserAccount(newUser) {
        const collectionName = 'users';
        var firebaseApp = commonUtil.CreateFirebaseApp();
        var db = firebaseApp.firestore();
        var userAcc = db.collection(collectionName);
        return userAcc.where("userId", "==", this.userId)
            .get()
            .then(records => {
                if (records.empty) {
                    let data = {
                        userId: this.userId,
                        firstName: newUser.firstName,
                        lastName: newUser.lastName,
                        createdTimeStamp: new Date(),
                        lastUpdateTimeStamp: new Date()
                    }
                    return userAcc.add(data);
                } else {
                    throw new customException.UserAlreadyExistsException()
                }
            })
            .then(newUserAccountRef => {
                return userChannels.doc(newUserAccountRef.id).get();
            })
            .then(newUserAccount => {
                return newUserAccount.data();
            })
            .catch(err => {
                throw err;
            })
    }

    updateUserAccount(newUser){

    }
}