'use strict';

const router=require('express').Router({mergeParams: true});

let userController=require('../controllers/userController');

//Get the user profile information
router.route('/:userid')
.get(userController.getUserInformation);

//Create a user profile
router.route('/:userid')
.post(userController.createUserProfile);

module.exports = router;