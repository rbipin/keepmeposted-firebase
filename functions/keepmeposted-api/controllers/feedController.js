/* eslint-disable promise/always-return */
'use strict';

const xml2js = require('xml2js');
const fetchHttp = require('node-fetch');
const customException = require('../common/exceptions/customException');
const FeedModel = require('../models/domain/feedModel');

//Enum for XML Feed Types
const feedTypeEnum = {
  Atom: 'atom+xml',
  RSS: 'rss+xml',
  Unknown: 'unknown'
}

//import xml parser
exports.getFeed = (request, response) => {
  try {
    const rssFeedSourceUrl = request.body.feedUrl; //Get Feed URL
    const ifModifiedSince = request.body.ifModifiedSince; //Get If-Modified-Since; For Conditional GET
    const ifNoneMatch = request.body.ifNoneMatch; //Get If-None-Match; For Conditional GET
    let httpOptions = {};
    let originResponseHeader = {};
    //Throw Error in case the RSS URL is empty
    if (rssFeedSourceUrl === undefined || rssFeedSourceUrl === null || rssFeedSourceUrl.length === 0) {
      throw new customException.InvalidFeedUriException();
    }
    //Verify if the Conditional GET headers needs to be passed
    if ((ifModifiedSince !== undefined && ifModifiedSince.length !== 0) ||
      (ifNoneMatch !== undefined && ifNoneMatch.length !== 0)) {
      httpOptions = {
        method: 'GET',
        headers: {
          "If-Modified-Since": ifModifiedSince,
          "If-None-Match": ifNoneMatch
        }
      };
    } else {
      httpOptions = {
        method: 'GET'
      };
    }

    //Get the XML raw feed and convert it into Json Feed
    let feedType = feedTypeEnum.Unknown;
    getHttpContent(rssFeedSourceUrl, httpOptions) //Get the feed from the URL
      .then(res => {
        const responseType = res.headers.get('content-type').split('/')[1].split(';')[0];
        originResponseHeader = {
          'Last-Modified': res.headers.get('Last-Modified'),
          'ETag': res.headers.get('ETag')
        };

        switch (responseType) {
          case 'atom+xml':
            feedType = feedTypeEnum.Atom;
            break;
          case 'rss+xml':
            feedType = feedTypeEnum.RSS;
            break;
          default:
            //return a custom exception in case it is not an expected header
            throw customException.InvalidFeedUriException();
        }
        return res.text();
      }) //Read the header type and also the main feed content from the response
      .then(xmlFeed => {
        return convertRawFeedToJson(xmlFeed, feedType);
      }) // Conver the XML Raw Feed to JSON
      .then(rssFeedAsJson => {
        response.set(originResponseHeader).send(rssFeedAsJson);
      }) //Send back the JSON feed
      .catch(err => {
        throw err;
      }); //Handle error in Catch

  } catch (exception) {
    switch (exception.constructor) {
      case customException.InvalidFeedUriException:
        response.status(exception.status).send(`${exception.message}`);
        break;
      default:
        response.status(500).send(`Server Error, ${exception.message}`);
        break;
    }
  }
};


/*Get the RSS or Atom feed from the URL in JSON Format */
function getHttpContent(rssFeedSourceUrl, httpOptions) {
  //Send request to the remote url and get the content
  return fetchHttp(rssFeedSourceUrl, httpOptions);
}

/*Convert to Raw XML feed to JSON format*/
function convertRawFeedToJson(rawFeed, feedType) {
  var jsonFeed = {};
  //XML parser settings
  let xmlParserSettings = {
    normalize: true,
    normalizeTags: true,
    explicitRoot: true,
    explicitArray: false,
    ignoreAttrs: false,
    mergeAttrs: true
  };
  //Conver the XML feed to JSON Format
  xml2js.parseString(rawFeed, xmlParserSettings, (err, result) => {
    if (err !== undefined && err !== null) {
      throw err;
    }
    switch (feedType) {
      case feedTypeEnum.Atom:
        jsonFeed = new FeedModel('atom', result.feed);
        break;
      case feedTypeEnum.RSS:

      //Object.entries() is not supported in node 6
        if (Object.entries(result.rss).length !== 0)
          jsonFeed = new FeedModel('rss', result.rss.channel);
        else
          jsonFeed = new FeedModel('rss', '');
        break;
    }
  });
  return jsonFeed;
}